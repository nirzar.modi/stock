import yfinance as yf
import numpy as np
import pandas as pd
from statsmodels.tsa.api import VAR
import streamlit as st
# from datetime import timedelta


import statsmodels.api as sm

tickers = ["AAPL", "MSFT", "GOOG", "AMZN", "META"]

# Import data from Yahoo Finance and set group_by to 'ticker'
data = yf.download(tickers, start="2020-01-01", end="2023-03-22", group_by='ticker')
data = data.reset_index()
df = pd.DataFrame()

# Reset the index to add the date column as a separate column
for ticker in tickers:
    df[ticker] = data[ticker]['Close']

date1 = data['Date']
df = pd.concat([date1, df], axis=1)
df.to_csv('stocks.csv')
df = pd.read_csv('stocks.csv', parse_dates=['Date'], index_col='Date')

# Select the columns to use for forecasting
columns = ['AAPL', 'MSFT', 'GOOG', 'AMZN','META']
dataf = df[columns]

# modell = sm.tsa.VAR(df)

# res = modell.fit(maxlags = 2, ic = 'bic')

# st.write(res.summary())
# Fit a VAR model to the data
model = VAR(dataf)
results = model.fit(75)

# model = sm.tsa.VAR(dataf)
# results = model.fit(50)


# Define a function to make predictions
def predict(start, end):
    # Create a date range based on the start and end dates
    date_range = pd.date_range(start=start, end=end)

    # Use the model to make predictions for the date range
    predictions = results.forecast(dataf.values[-results.k_ar:], len(date_range))

    # Convert the predictions to a DataFrame and set the column names
    preds_df = pd.DataFrame(predictions, index=date_range, columns=columns)

    # Return the predictions DataFrame
    return preds_df