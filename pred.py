import streamlit as st
import pandas as pd
from plotly import graph_objs as go
import yfinance as yf
# import matplotlib.pyplot as plt


# Import the predict function from the previous code block
from var_model import predict

tickers=("AAPL","MSFT","GOOG","AMZN","META")


# Set the title of the app
st.title("Future Stock Price Predictions")

# Add a short description
st.write("Enter a start and end date to get predictions.")

selected_stock = st.selectbox("Select stock for prediction",tickers)

# Add input fields for the start and end dates
start_date = st.date_input("Start date")
end_date = st.date_input("End date")

# Add a button to make the predictions
if st.button("Get Predictions"):
    # Call the predict function with the input dates    

    with st.container():
        st.write("---")
        left_col, right_col = st.columns((2))

        with left_col:
            predictions = predict(start_date, end_date)

            st.subheader("Predicted data")

            # Display the predicted values in a table
            st.write(predictions[selected_stock])

            # ss = predictions[selected_stock]                      
           
            
        with right_col:
            def load_data(ticker):
                data = yf.download(ticker,start_date,end_date)
                data.reset_index(inplace=True)
                return data

            data = load_data(selected_stock)
            close_col = data[['Date','Close']]
            st.subheader('Raw Data')
            st.write(close_col)
    

    # closee = close_col['Close']
    # # st.write(closee)

    # diff = close_col['Close'] - predictions[selected_stock]
    # st.write(diff)

    # pred_closee = predictions['selected_stock']
    # st.write(pred_closee)
    # diff = predictions[selected_stock] - closee

    # st.subheader("Difference between actual and predicted:")
    # st.write(diff)              

predictions = predict(start_date, end_date)
ss = predictions[selected_stock]
def plot_raw_data():
    fig = go.Figure()
    fig.add_trace(go.Scatter(x= predictions.index, y= ss, name="stock_open"))
    fig.layout.update(title_text='Time Series data with Range slider', xaxis_rangeslider_visible=True)
    st.plotly_chart(fig)
plot_raw_data()


st.write("Thanks for using this application!!")

# # close_col = close_col.reset_index(drop=True)
# # close_col= close_col.drop(close_col.columns[0], axis=0)
# abc= ss.merge(close_col[close_col.columns(1)])
# #close_col = close_col.reset_index(drop = True)

# raw = st.write(abc)

# st.write(predictions[selected_stock])
# predictions = predict(start_date,end_date)
# ss = predictions[selected_stock]

# merge = pd.merge(ss,raw,on = "Close")
# st.write(merge)